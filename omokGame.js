"use strict";

function OmokGame() {
	this.mode = "";
	this.firstMove = Math.floor(Math.random() * 2) + 1;;
	this.inGame = false;
	this.disconnected = true;
	this.owner = {win:0, loss:0, tie:0};
	this.visitor = {win:0, loss:0, tie:0};
	this.board = new Array(225);
	this.omokHistory = [];
}

module.exports = OmokGame;

OmokGame.prototype.debugInfo = function(url) {
	let ret = "DEBUG INFO FOR ROOM" + url + "\r\nFIRSTMOVE = " + this.firstMove + "\r\nBOARD STATE: \r\n"
	for (let i = 0; i < 225; i++) {
		if (i % 15 == 0) ret += "\r\n";
		ret += this.board[i];
	}
	return ret;
}

// Returns the current state of the board in hexadecimal triplets form
OmokGame.prototype.currentState = function() {
	let ret = "";
	for (let i = 0; i < this.omokHistory.length; i++) {
		let x = Math.floor(this.omokHistory[i] / 15);
		let y = this.omokHistory[i] % 15;
		ret += x.toString(16) + y.toString(16) + this.board[this.omokHistory[i]].toString(16);
	}
	return ret;
}

OmokGame.prototype.resetBoard = function() {
	this.board.fill(0);
	this.omokHistory = [];
}

// Returns the no. of horizontally connected pieces, return -1 if unblocked three
OmokGame.prototype.connHORZ = function(x, y, type) {
	let open = false;
	let connected = 0;
	let i = x;
	while (this.board[i*15 + y] == type) {
		i++;
		connected++;
		if (i >= 15) break;
	}
	if (i < 15 && this.board[i*15 + y] == 0) open = true;
	if (x > 0) {
		i = x;
		while (this.board[(i-1)*15 + y] == type) {
			i--;
			connected++;
			if (i <= 0) break;
		}
		if (i > 0 && connected == 3 && open && this.board[(i-1)*15 + y] == 0) return -1;
	}
	return connected;
}

// Returns the no. of vertically connected pieces, return -1 if unblocked three
OmokGame.prototype.connVERT = function(x, y, type) {
	let open = false;
	let connected = 0;
	let j = y;
	while (this.board[x*15 + j] == type) {
		j++;
		connected++;
		if (j >= 15) break;
	}
	if (j < 15 && this.board[x*15 + j] == 0) open = true;
	if (y > 0) {
		j = y;
		while (this.board[(x*15) + (j-1)] == type) {
			j--;
			connected++;
			if (j <= 0) break;
		}
		if (j > 0 && connected == 3 && open && this.board[x*15 + (j-1)] == 0) return -1;
	}
	return connected;
}

// Top left to bottom right
OmokGame.prototype.connTLBR = function(x, y, type) {
	let open = false;
	let connected = 0;
	let i = x;
	let j = y;
	while (this.board[i*15 + j] == type) {
		i++;
		j++;
		connected++;
		if (i >= 15 || j >= 15) break;
	}
	if (i < 15 && j < 15 && this.board[i*15 + j] == 0) open = true;
	if (x > 0 && y > 0) {
		i = x;
		j = y;
		while (this.board[(i-1)*15 + (j-1)] == type) {
			i--;
			j--;
			connected++;
			if (i <= 0 || j <= 0) break;
		}
		if (i > 0 && j > 0 && connected == 3 && open && this.board[(i-1)*15 + (j-1)] == 0) return -1;
	}
	return connected;
}

// From bottom left to top right
OmokGame.prototype.connBLTR = function(x, y, type) {
	let open = false;
	let connected = 0;
	let i = x;
	let j = y;
	while (this.board[i*15 + j] == type) {
		i--;
		j++;
		connected++;
		if (i <= 0 || j >= 15) break;
	}
	if (i > 0 && j < 15 && this.board[i*15 + j] == 0) open = true;
	if (x < 14 && y > 0) {
		i = x;
		j = y;
		while (this.board[(i+1)*15 + (j-1)] == type) {
			i++;
			j--;
			connected++;
			if (i >= 14 || j <= 0) break;
		}
		if (i < 14 && j > 0 && connected == 3 && open && this.board[(i+1)*15 + (j-1)] == 0) return -1;
	}
	return connected;
}

// Checks if the 3 x 3 is legal (it's only legal if it is used to block an opponent's 4)
// Returns true if legal, false if illegal, this function will set the value at (x,y) to the opponent's type
OmokGame.prototype.legalThreeThree = function(x, y, type) {
	let type2 = (type == 1 ? 2 : 1)
	this.board[x*15 + y] = type2;
	return (this.connHORZ(x, y, type2) == 5 || this.connVERT(x, y, type2) == 5 || this.connTLBR(x, y, type2) == 5 || this.connBLTR(x, y, type2) == 5);
}

// Called when a client tries to place a piece
OmokGame.prototype.setPiece = function(x, y, type) {
	if (this.board[x*15 + y] == 0) {
		this.board[x*15 + y] = type;

        // Check for three threes
        if (this.mode != "0") {
       		if (this.connHORZ(x, y, type) == -1) {
	            if (this.connVERT(x, y, type) == -1 || this.connTLBR(x, y, type) == -1 || this.connBLTR(x, y, type) == -1) {
	                if (!this.legalThreeThree(x, y, type)) {
						this.board[x*15 + y] = 0;
	                    return 3;
	                } else {
	                    this.board[x*15 + y] = type;
	                }
	            }
	        } else if (this.connVERT(x, y, type) == -1) {
	            if (this.connTLBR(x, y, type) == -1 || this.connBLTR(x, y, type) == -1) {
	                if (!this.legalThreeThree(x, y, type)) {
						this.board[x*15 + y] = 0;
	                    return 3;
	                } else {
	                    this.board[x*15 + y] = type;
	                }
	            }
	        } else if (this.connTLBR(x, y, type) == -1) {
	            if (this.connBLTR(x, y, type) == -1) {
	                if (!this.legalThreeThree(x, y, type)) {
						this.board[x*15 + y] = 0;
	                    return 3;
	                } else {
	                    this.board[x*15 + y] = type;
	                }
	            }
	        }
        }
	
        // Check for 6's
        if (this.connHORZ(x, y, type) > 5 || this.connVERT(x, y, type) > 5 || 
                this.connTLBR(x, y, type) > 5 || this.connBLTR(x, y, type) > 5) {
            this.board[x*15 + y] = 0;
            if (this.mode == "0") {
            	this.omokHistory.push(x*15 + y);
            	return 5; 
            } else {
            	return 6;
            }
        }

        this.omokHistory.push(x*15 + y);
        if (this.connHORZ(x, y, type) == 5 || this.connVERT(x, y, type) == 5 || 
                this.connTLBR(x, y, type) == 5 || this.connBLTR(x, y, type) == 5) {
        	return 5;
        }
        return 0;
	} else {
		return -1;
	}
}