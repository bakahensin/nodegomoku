var Opcode = {
	JOINROOM: "0",
	CHAT: "1",
	READY: "2",
	START: "3",
	STONE: "4",
	FINISH: "5",
	THREETHREE: "6",
	OVERLINE: "7",
	DISCONNECT: "8",
	RECONNECT: "9",
	REFRESH: "A",
	UNK: "B"
};	

var url = window.location.pathname.slice(6);
var socket = io.connect("/omok");
var rules = "";
var enableActions = true;
var canPlace = false;
var inGame = false;
var firstMove;
var stoneLength = 40;
var omokRole = 0;
var stoneType = 0;
var owner = {win: 0, loss: 0, tie: 0};
var visitor = {win: 0, loss: 0, tie: 0};
var omokBoard = document.getElementById("omokBoard");
var infoWrapper = document.getElementById("infoWrapper");
var players = document.getElementById("players");
var readyStart = document.getElementById("readyStart");
var messages = document.getElementById("messages");

socket.on(Opcode.JOINROOM, function(gameMode, ownerInfo, visitorInfo, isSpectator) {
	if (rules == "") {
		rules = gameMode;
		if (rules == "0") {
			displayMessage("You have entered a private room with Basic Gomoku Rules.");
		} else if (rules == "1") {
			displayMessage("You have entered a private room with GMS Omok Rules.");
		} else {
			displayMessage("You have entered a private room with Renju Rules.");
		}
	}
	displayMessage("You can type \"/rules\" for a brief outline of the rules for this room and \"/help\" for a list of available commands.");
	if (visitorInfo["name"] == null) {
		displayMessage("Share the URL below with somebody to play against:");
		displayMessage(window.location.toString());
		omokRole = 1;
		readyStart.innerHTML = "Start";
	} else if (omokRole == 0 && !isSpectator) {
		readyStart.disabled = false;
		omokRole = 2;
	}
	owner = ownerInfo;
	visitor = visitorInfo;
	displayPlayers(false);
	if (isSpectator) {
		displayMessage("A spectator has joined the room.");
	} else if (visitor["name"] != null) {
		displayMessage(visitor["name"] + " (visitor) has joined the room.");
	}
});

socket.on(Opcode.CHAT, function(player, message) {
	if (player == 1) {
		displayChat(owner["name"] + " : " + message);
	} else if (player == 2) {
		displayChat(visitor["name"] + " : " + message);
	} else {
		displayChat("Spectator : " + message)
	}
});

socket.on(Opcode.READY, function(isReady) {
	if (omokRole == 1) {
		if (isReady) {
			readyMessage("Your opponent is ready, press the Start button to play!");
			readyStart.disabled = false;
		} else {
			readyMessage(null);
			readyStart.disabled = true;
		}
	} else {
		if (isReady) {
			readyStart.innerHTML = "Unready";
		} else {
			readyStart.innerHTML = "Ready";
		}

	}
});

socket.on(Opcode.START, function(firstMove) { // owner = 1, visitor = 2
	initialiseBoard();
	readyStart.disabled = true;
	inGame = true;
	if (firstMove == 1) {
		displayMessage("The game has started with " + owner["name"] + " (owner) as black and " + visitor["name"] + " (visitor) as white.");
	} else if (firstMove == 2) {
		displayMessage("The game has started with " + visitor["name"] + " (visitor) as black and " + owner["name"] + " (owner) as white.");
	}
	if (firstMove == omokRole) {
		stoneType = 1;
		canPlace = true;
		displayMessage("Your move.");
	} else {
		stoneType = 2;
	}
	displayPlayers(true);
});

socket.on(Opcode.STONE, function(x, y, type) {
	setStone(x, y, type);
});

socket.on(Opcode.FINISH, function(winner) { // owner = 1, visitor = 2
	switch (winner) {
		case 1:
			owner["win"]++;
			visitor["loss"]++;
			displayMessage(owner["name"] + " (owner) has won the match!");
			break;
		case 2:
			owner["loss"]++;
			visitor["win"]++;
			displayMessage(visitor["name"] + " (visitor) has won the match!");
			break;
		case 3:
			owner["tie"]++;
			visitor["tie"]++;
			displayMessage("The game ended in a draw.");
			break;
		case -1:
			owner["loss"]++;
			visitor["win"]++;
			displayMessage(owner["name"] + " (owner) has forfeited the match!");
			break;
		case -2:
			owner["win"]++;
			visitor["loss"]++;
			displayMessage(visitor["name"] + " (visitor) has forfeited the match!");
	}
	displayPlayers(false);
	if (omokRole == 2) {
		readyStart.disabled = false;
		readyStart.innerHTML = "Ready";
	}
	inGame = false;
	canPlace = false;
});

socket.on(Opcode.THREETHREE, function() {
	displayMessage("Illegal 3x3.");
	canPlace = true;
});

socket.on(Opcode.OVERLINE, function() {
	displayMessage("You cannot have more than 5 stones in a row.");
	canPlace = true;
});

socket.on(Opcode.UNK, function() {
	displayMessage("An unknown error has occurred.");
	canPlace = true;
});

socket.on(Opcode.REFRESH, function(currentState) {
	for (var i = 0; i < currentState.length; i += 3) {
		setStone(parseInt(currentState[i], 16), parseInt(currentState[i+1], 16), parseInt(currentState[i+2]), 16);
	}
});

socket.on(Opcode.DISCONNECT, function(player) {
	if (inGame) {
		if (player == 1) {
			displayMessage(owner["name"] + "(owner) has been disconnected, please wait while we attempt to restore the connection.");
		} else if (player == 2) {
			displayMessage(visitor["name"] + "(visitor) has been disconnected, please wait while we attempt to restore the connection.");
		}
	} else {
		if (player == 1) {
			displayMessage(owner["name"] + "(owner) has left.");
			displayMessage("The room will now close due to the owner leaving.");
			displayMessage("You will be automatically redirected to the lobby.");
			setTimeout(function() {
				window.location.href = "/";
			}, 5000);
		} else if (player == 2) {
			displayMessage(visitor["name"] + "(visitor) has left.");
			visitor = {win: 0, loss: 0, tie: 0};
			displayPlayers();
		}
	}
});

socket.on(Opcode.RECONNECT, function(success) {
	if (success) {
		displayMessage("Reconnected!");
		enableActions = true;
	} else {
		displayMessage("The room you are trying to reconnect to does not exist.");
		displayMessage("You will be automatically redirected to the lobby.");
		setTimeout(function() {
			window.location.href = "/";
		}, 5000);
	}
});

socket.on("disconnect", function() {
	if (inGame) {
		enableActions = false;
		reconnect(0);
	} else {
		displayMessage("You have been disconnected.");
		displayMessage("You will be automatically redirected to the lobby.");
		setTimeout(function() {
			window.location.href = "/";
		}, 5000);
	}

});

function reconnect(counter) {
	if (counter < 5) {
		displayMessage("You have been disconnected. Attempting to reconnect...");
		socket.connect();
		if (socket.id != null) {
			socket.emit(Opcode.RECONNECT, url);
		} else {
			setTimeout(function() {
				reconnect(counter + 1);
			}, 2000);
		}
	} else {
		displayMessage("Reconnect failed, you will be automatically redirected to the lobby.");
		setTimeout(function() {
			window.location.href = "/";
		}, 5000);
	}
}

function displayPlayers(inGame) {
	while (players.hasChildNodes()) players.removeChild(players.lastChild);
	var listItem = document.createElement("li");
	if (inGame && ((omokRole == 1 && canPlace) || (omokRole == 2 && !canPlace))) listItem.style.fontWeight = "bold";
	listItem.appendChild(document.createTextNode(owner["name"] + " [W: " + owner["win"] + ", L: " + owner["loss"] + ", T: " + owner["tie"] + "]"));
	players.appendChild(listItem);
	if (visitor["name"] != null) {
		listItem = document.createElement("li");
		if (inGame && ((omokRole == 2 && canPlace) || (omokRole == 1 && !canPlace))) listItem.style.fontWeight = "bold";
		listItem.appendChild(document.createTextNode(visitor["name"] + " [W: " + visitor["win"] + ", L: " + visitor["loss"] + ", T: " + visitor["tie"] + "]"));
		players.appendChild(listItem);
	}
}

function displayMessage(string) {
	var listItem = document.createElement("li");
	listItem.className = "systemMessage";
	listItem.appendChild(document.createTextNode(string));
	messages.appendChild(listItem);
	listItem.scrollIntoView();
}

function displayChat(string) {
	var listItem = document.createElement("li");
	listItem.appendChild(document.createTextNode(string));
	messages.appendChild(listItem);
	listItem.scrollIntoView();
}

function readyMessage(string) {
	if (string != null) {
		var listItem = document.createElement("li");
		listItem.id = "readyMessage";
		listItem.appendChild(document.createTextNode(string));
		messages.appendChild(listItem);
		listItem.scrollIntoView();
	} else {
		var readyMessage = document.getElementById("readyMessage");
		if (readyMessage != null && readyMessage.parentElement != null) {
			readyMessage.parentElement.removeChild(readyMessage);
		}
	}
}

function initialiseBoard() {
	while (omokBoard.hasChildNodes()) omokBoard.removeChild(omokBoard.lastChild);
	var tHead, th, tr, td;
	tHead = document.createElement("thead");
	tHead.appendChild(document.createElement("th"));
	omokBoard.appendChild(tHead);
	for (var i = 0; i < 15; i++) {
		th = document.createElement("th");
		th.appendChild(document.createTextNode(String.fromCharCode(65 + i)));
		tHead.appendChild(th);

		tr = document.createElement("tr");
		tr.x = i;
		th = document.createElement("th");
		th.appendChild(document.createTextNode(1 + i));
		tr.appendChild(th);

		for (var j = 0; j < 15; j++) {
			td = document.createElement("td");
			td.y = j;
			td.onclick = function(e) {
				if (omokRole == 1 || omokRole == 2) {
					if (enableActions) {
						if (canPlace) {
							socket.emit(Opcode.STONE, e.target.parentElement.x, e.target.y, omokRole);
							canPlace = false;
						} else {
							displayMessage("It's not your turn yet.");
						}
					} else {
						displayMessage("You cannot do that right now.");
					}
				}
			};
			tr.appendChild(td);
		}

		th = document.createElement("th");
		tr.appendChild(th);

		omokBoard.appendChild(tr);
	}
	tHead.appendChild(document.createElement("th"));
	tr = document.createElement("tr");
	for (var i = 0; i < 17; i++) tr.appendChild(document.createElement("th"));
	omokBoard.appendChild(tr);
}

function setStone(x, y, type) {
	console.log(x + ", " + y);
	var square = omokBoard.children[x + 1].children[y + 1];
	var stone = document.createElement("img");
	if (type == 1) {
		stone.src = "/images/blackL.png";
		var lastPlaced = document.getElementById("lastPlaced");
		if (lastPlaced != null) {
			lastPlaced.src = "/images/white.png";
			lastPlaced.id = null;
		}
	} else if (type == 2) {
		stone.src = "/images/whiteL.png";
		var lastPlaced = document.getElementById("lastPlaced");
		if (lastPlaced != null) {
			lastPlaced.src = "/images/black.png";
			lastPlaced.id = null;
		}
	} else {
		return;
	}
	stone.id = "lastPlaced";
	stone.style.width = stoneLength*0.85 + "px";
	stone.style.height = stoneLength*0.85 + "px";

	square.appendChild(stone);
	square.onclick = function() {
		displayMessage("You cannot place there.");
	}
	if (stoneType != 0 && type != stoneType) canPlace = true;
	displayPlayers(true);
}

readyStart.onclick = function() {
	if (enableActions) {
		if (omokRole == 1) {
			readyMessage(null);
			socket.emit(Opcode.START);
		} else {
			if (readyStart.innerHTML == "Ready") {
				socket.emit(Opcode.READY, true);
			} else {
				socket.emit(Opcode.READY, false);
			}
		}
	}
}

document.getElementById("messageBox").onsubmit = function(e) {
	e.preventDefault();
	var message = document.getElementById("messageInput");
	if (message.value == "/help") {
		displayMessage("");
		displayMessage("List of Available Commands");
		displayMessage(" - /help : display this message");
		displayMessage(" - /forfeit : forfeit the game");
		displayMessage(" - /rules : display the rules in the current room");
	} else if (message.value == "/rules") {
		displayMessage("");
		if (rules == "0") {
			displayMessage("Basic Gomoku Rules");
			displayMessage(" - 3x3 Allowed");
			displayMessage(" - 5 or more to Win");
		} else if (rules == "1") {
			displayMessage("GMS Omok Rules");
			displayMessage(" - 3x3 Disallowed");
			displayMessage(" - Overline Disallowed");
			displayMessage(" - Exactly 5 to Win");
		} else if (rules == "2") {
			displayMessage("Renju Rules");
			displayMessage(" - 3x3, 4x4 and Overline Disallowed as Black");
			displayMessage(" - Exactly 5 to Win as Black");
			displayMessage(" - 5 or more to Win as White");
			displayMessage(" - White wins if Black is forced to make a disallowed move");
		}
	} else if (message.value != "") {
		socket.emit(Opcode.CHAT, omokRole, message.value);
	}
	message.value = "";
}

initialiseBoard();

socket.emit(Opcode.JOINROOM, url, prompt("Please enter your name: ", "Anonymous"));
window.onbeforeunload = function(e) { if (inGame) return "You are currently in a match."; };