"use strict";

var port = 80;

var express = require("express");
var app = express();
var http = require("http").Server(app);
var io = require("socket.io")(http);
var omokGame = require("./omokGame.js");

var Opcode = {
	JOINROOM: "0",
	CHAT: "1",
	READY: "2",
	START: "3",
	STONE: "4",
	FINISH: "5",
	THREETHREE: "6",
	OVERLINE: "7",
	DISCONNECT: "8",
	RECONNECT: "9",
	REFRESH: "A",
	CONNERROR: "B"
};
var omokRooms = {};
var omokPlayers = {};

app.use(express.static("public"));

app.get("/", function(req, res) {
	res.sendFile(__dirname + "/index.html");
});

app.get("/omok", function(req, res) {
	res.sendFile(__dirname + "/omokCreate.html");
});

app.get("/about", function(req, res) {
	res.sendFile(__dirname + "/about.html");
});

app.get("/omok/:url", function(req, res) {
	if (omokRooms[req.params.url] == null) {
		res.redirect("/");
	} else {
		res.sendFile(__dirname + "/omok.html");
	}
});

var omokIO = io.of("/omok");
omokIO.on("connection", function(socket) {
	console.log(socket.id);
	// Lobby Operations
	socket.on("new", function(mode) {
		let url = Math.random().toString(36).substr(2);
		omokRooms[url] = new omokGame();
		omokRooms[url].mode = mode;
		omokIO.to(socket.id).emit("new", url);
		console.log("New room : " + url);
	});

	// Room Operations
	socket.on(Opcode.JOINROOM, function(url, name) {
		if (omokRooms[url] != null) {
			if (omokRooms[url].owner["name"] == null) {
				omokRooms[url].owner["name"] = (name != null && name != "") ? name : "Anonymous";
				socket.role = 1;
				console.log(omokRooms[url].owner["name"] + " (owner) has joined a room at /omok/" + url);
			} else if (omokRooms[url].visitor["name"] == null) {
				omokRooms[url].visitor["name"] = (name != null && name != "") ? name : "Anonymous";
				omokRooms[url].disconnected = false;
				socket.role = 2;
				console.log(omokRooms[url].visitor["name"] + " (visitor) has joined a room at /omok/" + url);
			} else {
				socket.role = 3;
				omokIO.to(socket.id).emit(Opcode.REFRESH, omokRooms[url].currentState());
			}
			socket.join(url);
			socket.room = url;
			omokIO.in(url).emit(Opcode.JOINROOM, omokRooms[url].mode, omokRooms[url].owner, omokRooms[url].visitor, socket.role == 3);
		} else {
			omokIO.to(socket.id).emit(Opcode.RECONNECT, false);
		}
	});

	socket.on(Opcode.CHAT, function(player, message) {
		if (message[0] != '/') {
			omokIO.in(socket.room).emit(Opcode.CHAT, player, message);
		} else if (message == "/forfeit") {
			if (omokRooms[socket.room] != null && omokRooms[socket.room].omokHistory.length > 0) {
				omokIO.in(socket.room).emit(Opcode.FINISH, -player);
				omokRooms[socket.room].firstMove = player == 1 ? 1 : 2;
				omokRooms[socket.room].inGame = false;
				if (player == 1) {
					omokRooms[socket.room].owner["win"]++;
					omokRooms[socket.room].visitor["loss"]++;
				} else if (player == 2) {
					omokRooms[socket.room].owner["loss"]++;
					omokRooms[socket.room].visitor["win"]++;
				}
			}
		} else if (message == "/tableflip") {
			omokIO.in(socket.room).emit(Opcode.CHAT, player, "(╯°□°）╯︵ ┻━┻");
		} else if (message == "/unflip") {
			omokIO.in(socket.room).emit(Opcode.CHAT, player, "┬─┬﻿ ノ( ゜-゜ノ)");
		} else if (message == "/oops") {
			omokIO.in(socket.room).emit(Opcode.CHAT, player, "(´･ω･｀)");
		}
		
	})

	socket.on(Opcode.READY, function(isReady) {
		omokIO.in(socket.room).emit(Opcode.READY, isReady);
	});

	socket.on(Opcode.START, function() {
		omokRooms[socket.room].resetBoard();
		omokRooms[socket.room].inGame = true;
		omokIO.in(socket.room).emit(Opcode.START, omokRooms[socket.room].firstMove);
	});

	socket.on(Opcode.STONE, function(x, y, player) {
		let type;
		if (omokRooms[socket.room].firstMove == player) {
			type = 1;
		} else {
			type = 2;
		}
		let status = omokRooms[socket.room].setPiece(x, y, type);
		switch (status) {
			case 0:
				omokIO.in(socket.room).emit(Opcode.STONE, x, y, type);
				//console.log(omokRooms[socket.room].debugInfo(socket.room));
				break;
			case 5:
				omokIO.in(socket.room).emit(Opcode.STONE, x, y, type);
				omokIO.in(socket.room).emit(Opcode.FINISH, player);
				omokRooms[socket.room].firstMove = player == 1 ? 2 : 1;
				omokRooms[socket.room].inGame = false;
				if (player == 1) {
					omokRooms[socket.room].owner["win"]++;
					omokRooms[socket.room].visitor["loss"]++;
				} else if (player == 2) {
					omokRooms[socket.room].owner["loss"]++;
					omokRooms[socket.room].visitor["win"]++;
				}
				break;
			case 3:
				omokIO.to(socket.id).emit(Opcode.THREETHREE);
				break;
			case 6:
				omokIO.to(socket.id).emit(Opcode.OVERLINE);
				break;
			default:
				omokIO.to(socket.id).emit(Opcode.UNK);
				break;
		}
	});

	socket.on(Opcode.RECONNECT, function(url) {
		if (omokRooms[url] != null && omokRooms[url].disconnected == true) {
			socket.join(url);
			socket.room = url;
			omokIO.to(socket.id).emit(Opcode.RECONNECT, true);
		} else {
			omokIO.to(socket.id).emit(Opcode.RECONNECT, false);
		}
	});

	socket.on("disconnect", function() {
		if (socket.room != null && omokRooms[socket.room] != null && socket.role != 3) {
			if (omokRooms[socket.room].disconnected) {
				console.log("All players disconnected, room \"" + socket.room + "\" removed.");
				omokRooms[socket.room] = null;
			} else {
				console.log("Player disconnect detected");
				omokRooms[socket.room].disconnected = true;
				omokIO.in(socket.room).emit(Opcode.DISCONNECT, socket.role);
				if (socket.role == 2 && !omokRooms[socket.room].inGame) omokRooms[socket.room].visitor = {win:0, loss:0, tie:0};
			}
		}
	});
});

http.listen(port, function() {
	console.log("Listening on *:" + port);
});