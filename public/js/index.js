var socket = io.connect("/omok");
socket.on("new", function(url) {
	window.location.href = "/omok/" + url;
});
document.getElementById("create").onclick = function() {
	socket.emit("new", document.getElementById("selected").getAttribute("mode"));
}
function changeMode(mode) {
	var selected = document.getElementById("selected");
	if (selected != null) selected.removeAttribute("id");
	mode.id = "selected";
}